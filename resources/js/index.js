const VOD_BASE_PATH = "https://twitch.tv/videos/";
const VOD_BASE_PATH_WWW = "https://www.twitch.tv/videos/";
const API_URL = "https://gql.twitch.tv/gql";
const btn_post = document.getElementById("submit_btn");
const url_out_element = document.getElementById("tocopy");
const btn_copy = document.getElementById("btn_copy");

btn_post.addEventListener("click", () => {
  const twitchVodLinkNotClean = document.getElementById("twitch_vod_link").value;
  const twitchVodLink = twitchVodLinkNotClean.replace("//m.", "//www.");
  // validate twitch vod link
  if (
    !twitchVodLink.startsWith(VOD_BASE_PATH) &&
    !twitchVodLink.startsWith(VOD_BASE_PATH_WWW)
  ) {
    throw new Error("Invalid twitch vod link");
  }
  const vodId = twitchVodLink.split("/").pop();

  const headers = {
    "Client-Id": "kimne78kx3ncx6brgo4mv6wki5h1ko",
  };

  const query = [{"operationName":"VideoPlayer_VODSeekbarPreviewVideo","variables":{"includePrivate":false,"videoID":`${vodId}`},"extensions":{"persistedQuery":{"version":1,"sha256Hash":"07e99e4d56c5a7c67117a154777b0baf85a5ffefa393b213f4bc712ccaf85dd6"}}}];

  fetch(API_URL, {
    method: "POST",

    body: JSON.stringify(query),
    headers,
  }).then(async (response) => {
    const jsonResponse = await response.json();
    if (jsonResponse[0].data?.video?.seekPreviewsURL) {
      const data = jsonResponse[0].data
      const seekPreviewsURL = data.video.seekPreviewsURL;
      const storyBoardIndex = seekPreviewsURL.indexOf("/storyboards");
      const urlWithoutStoryBoard = seekPreviewsURL.substring(
        0,
        storyBoardIndex
      );
      const m3uUrl = urlWithoutStoryBoard + "/chunked/index-dvr.m3u8";
      url_out_element.innerText = `${m3uUrl}`;
    }
  });
});

if(btn_copy) {
    btn_copy.addEventListener('click', docopy);
}

function docopy() {
    var range = document.createRange();
    var target = this.dataset.target;
    var fromElement = document.querySelector(target);
    var selection = window.getSelection();

    range.selectNode(fromElement);
    selection.removeAllRanges();
    selection.addRange(range);

    try {
        var result = document.execCommand('copy');
        if (result) {
            // La copie a réussi
            btn_copy.innerText = "Copié !";
        }
    }
    catch(err) {
        // Une erreur est surevnue lors de la tentative de copie
        alert(err);
    }

    selection = window.getSelection();

    if (typeof selection.removeRange === 'function') {
        selection.removeRange(range);
    } else if (typeof selection.removeAllRanges === 'function') {
        selection.removeAllRanges();
    }
}

document.getElementById('submit_btn').addEventListener('click', function() {
    var elements = document.getElementsByClassName('r-wrap-copy');
    for (var i = 0; i < elements.length; i++) {
      var element = elements[i];
      element.style.display = 'block';
    }
});